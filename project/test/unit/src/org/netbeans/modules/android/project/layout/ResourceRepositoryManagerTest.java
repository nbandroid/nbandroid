package org.netbeans.modules.android.project.layout;

import com.android.ide.common.resources.configuration.LanguageQualifier;
import com.android.ide.common.resources.configuration.RegionQualifier;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.core.sdk.DalvikPlatformManager;
import org.netbeans.modules.android.project.AndroidProject;
import org.netbeans.modules.android.project.AndroidTestFixture;

/**
 *
 * @author radim
 */
public class ResourceRepositoryManagerTest {
  
  private static AndroidTestFixture fixture;
  private static AndroidProject homePrj;
  private static DalvikPlatform testedPlatform;

  @BeforeClass
  public static void setUpClass() throws Exception {
    fixture = AndroidTestFixture.create().withProject("Home", "samples/android-8/Home");
    homePrj = (AndroidProject) fixture.getProject("Home");

    testedPlatform = DalvikPlatformManager.getDefault().findPlatformForTarget("android-8");
  }

  @AfterClass
  public static void delete() {
    fixture.tearDown();
  }

  @Test
  public void testGetLocaleConfigs() throws Exception {
    List<ResourceRepositoryManager.LocaleConfig> result = ResourceRepositoryManager.getLocaleConfigs(homePrj);
    assertEquals(8, result.size());
    assertTrue(result.contains(new ResourceRepositoryManager.LocaleConfig(
        new LanguageQualifier("cs"), 
        new RegionQualifier(RegionQualifier.FAKE_REGION_VALUE))));
    assertTrue(result.contains(new ResourceRepositoryManager.LocaleConfig(
        new LanguageQualifier("de"), 
        new RegionQualifier("DE"))));
    assertTrue(result.contains(new ResourceRepositoryManager.LocaleConfig(
        new LanguageQualifier("de"), 
        new RegionQualifier(RegionQualifier.FAKE_REGION_VALUE))));
    assertTrue(result.contains(new ResourceRepositoryManager.LocaleConfig(
        new LanguageQualifier(LanguageQualifier.FAKE_LANG_VALUE), 
        new RegionQualifier(RegionQualifier.FAKE_REGION_VALUE))));
  }
  
  @Test
  public void testGetLocaleConfigsNoPrj() throws Exception {
    List<ResourceRepositoryManager.LocaleConfig> result = ResourceRepositoryManager.getLocaleConfigs(null);
    assertEquals(1, result.size());
    System.err.println("locale: " + result.get(0));
    assertTrue(result.contains(ResourceRepositoryManager.LocaleConfig.theOnlyLocale()));
  }
}
