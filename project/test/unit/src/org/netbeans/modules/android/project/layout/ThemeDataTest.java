package org.netbeans.modules.android.project.layout;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author radim
 */
public class ThemeDataTest {
  
  @Test
  public void testCompareTo() {
    assertTrue(new ThemeData("CustomTheme", true).compareTo(new ThemeData("Theme", false)) < 0);
    assertTrue(new ThemeData("Theme", false).compareTo(new ThemeData("CustomTheme", true)) > 0);
    assertTrue(new ThemeData("Theme", false).compareTo(new ThemeData("Theme", false)) == 0);
    assertTrue(new ThemeData("ATheme", false).compareTo(new ThemeData("BTheme", false)) < 0);
    assertTrue(new ThemeData("BTheme", false).compareTo(new ThemeData("ATheme", false)) > 0);
    assertTrue(new ThemeData("CustomTheme", true).compareTo(new ThemeData("CustomTheme", true)) == 0);
    assertTrue(new ThemeData("ATheme", true).compareTo(new ThemeData("BTheme", true)) < 0);
    assertTrue(new ThemeData("BTheme", true).compareTo(new ThemeData("ATheme", true)) > 0);
  }
}
