package org.nyerel.nbandroid.logcat;

import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.IDevice;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 *
 * @author radim
 */
public class LogReaderTest {
    
    @Test
    public void testParsing() {
        IDevice d = mock(IDevice.class);
        Client c = mock(Client.class);
        ClientData cd = mock(ClientData.class);
        when(d.getSerialNumber()).thenReturn("123");
        when(d.getClients()).thenReturn(new Client[] {c});
        when(c.getClientData()).thenReturn(cd);
        when(cd.getPid()).thenReturn(13128);
        
        LogReader reader = new LogReader();
        LogEventInfo lei = reader.parseLine(d, "[ 10-19 21:24:34.441 13128:0x548b E/TalkProvider ]");
        assertNotNull(lei);
        assertEquals("parsed w/ time " + lei, "21:24:34.441", lei.getTime());
        
        // jellybean format
        lei = reader.parseLine(d, "[ 10-19 19:39:15.028   151:  165 I/PackageManager ]");
        assertNotNull(lei);
        assertEquals("parsed w/ time " + lei, "19:39:15.028", lei.getTime());
    }
}
