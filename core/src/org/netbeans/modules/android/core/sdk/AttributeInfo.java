/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.netbeans.modules.android.core.sdk;

import com.android.utils.Pair;

/**
 * Object that can be included in Android XML file to describe view or view layout element.
 *
 * @author radim
 */
public interface AttributeInfo {

  /** An attribute format, e.g. string, reference, float, etc. */
  public enum Format {
    STRING,
    BOOLEAN,
    INTEGER,
    FLOAT,
    REFERENCE,
    COLOR,
    DIMENSION,
    FRACTION,
    ENUM,
    FLAG
  }

  String getName();

  String getDescription();

  Iterable<Format> getFormats();

  Iterable<Pair<String, Integer>> getEnumValues();

  Iterable<Pair<String, Integer>> getFlagValues();
}
