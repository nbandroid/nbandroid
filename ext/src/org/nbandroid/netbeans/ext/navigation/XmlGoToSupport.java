package org.nbandroid.netbeans.ext.navigation;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nullable;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;
import javax.swing.text.Document;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.source.ClasspathInfo;
import org.netbeans.api.java.source.ElementHandle;
import org.netbeans.api.java.source.SourceUtils;
import org.netbeans.api.lexer.Token;
import org.netbeans.api.lexer.TokenHierarchy;
import org.netbeans.api.lexer.TokenSequence;
import org.netbeans.api.progress.ProgressUtils;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.xml.lexer.XMLTokenId;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.api.ResourceRef;
import org.netbeans.modules.android.project.api.AndroidClassPath;
import org.netbeans.modules.android.project.api.AndroidFileTypes;
import org.openide.filesystems.FileObject;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;

public class XmlGoToSupport {

  public interface DependencyResolver {
    ClasspathInfo cpInfoForXmlDocument(Document doc);
    @Nullable DalvikPlatform platform(Document doc);
    FileObject findFile(final ElementHandle<? extends Element> handle, final ClasspathInfo cpInfo);
  }
  
  private static class DefaultDependencyResolver implements DependencyResolver {

    @Override
    public ClasspathInfo cpInfoForXmlDocument(Document doc) {
      final FileObject fo = EditorUtilities.getFileObject(doc);
      if (fo == null) {
        return null;
      }
      Project p = FileOwnerQuery.getOwner(fo);
      if (p == null) {
        return null;
      }
      AndroidClassPath aCp = p.getLookup().lookup(AndroidClassPath.class);
      if (aCp == null) {
        return null;
      }
      return ClasspathInfo.create(
          aCp.getClassPath(ClassPath.BOOT), 
          aCp.getClassPath(ClassPath.COMPILE), 
          aCp.getClassPath(ClassPath.SOURCE));
    }

    @Override
    public FileObject findFile(ElementHandle<? extends Element> handle, ClasspathInfo cpInfo) {
      return SourceUtils.getFile(handle, cpInfo);
    }

    @Override
    public DalvikPlatform platform(Document doc) {
      return null;
    }
  }
  
  private final DependencyResolver cpResolver;
  private final AndroidFileTypes fileTypes;
  private final UiUtilsCaller callback;

  @VisibleForTesting XmlGoToSupport(DependencyResolver cpResolver, AndroidFileTypes fileTypes, UiUtilsCaller callback) {
    this.cpResolver = cpResolver;
    this.fileTypes = fileTypes;
    this.callback = callback;
  }
  
  XmlGoToSupport(UiUtilsCaller callback) {
    this(new DefaultDependencyResolver(), Lookup.getDefault().lookup(AndroidFileTypes.class), callback);
  }
  
    
  public String getGoToElementTooltip(final Document doc, final int offset) {
    final FileObject fo = EditorUtilities.getFileObject(doc);

    if (fo == null) {
      return null;
    }

    GoToContext refSpan = getIdentifierSpan(doc, offset);
    if (refSpan != null) {
      // TODO should be getTooltip()
      return refSpan.targetName();
    }
    return null;
  }
    
  private void performGoTo(final Document doc, final int offset) {
    final AtomicBoolean cancel = new AtomicBoolean();
    ProgressUtils.runOffEventDispatchThread(new Runnable() {
      @Override
      public void run() {
        performGoToImpl(doc, offset, cancel);
      }
    }, NbBundle.getMessage(XmlGoToSupport.class, "LBL_GoToResource"), cancel, false);
  }

  private void performGoToImpl(final Document doc, final int offset, final AtomicBoolean cancel) {
    final FileObject fo = EditorUtilities.getFileObject(doc);
    if (fo == null) {
      return;
    }
    GoToContext refSpan = getIdentifierSpan(doc, offset);
    if (refSpan != null) {
      if (cancel.get()) {
        return;
      }

      boolean openSucceeded = refSpan.open(callback, fo);
      if (!openSucceeded) {
        callback.warnCannotOpen("Cannot open resource " + refSpan.targetName());
      }
    }
  }

  public void goTo(final Document doc, final int offset) {
    performGoTo(doc, offset);
  }
    
  private static final Set<XMLTokenId> USABLE_TOKEN_IDS = EnumSet.of(XMLTokenId.VALUE);
  private static final Set<String> MANIFEST_TAGS_WIHT_CLASSES = Sets.newHashSet(
      "<application",
      "<provider",
      "<service",
      "<receiver",
      "<activity",
      "<activity-alias",
      "<instrumentation"
      );
  
  // @[<package_name>:]<resource_type>/<resource_name>
  private static final Pattern RES_REF_EXPR = Pattern.compile(
      "\"@(string|drawable|color|xml)/(\\p{Alpha}\\w*)\"");
    
  public GoToContext getIdentifierSpan(final Document doc, final int offset) {
    final FileObject fObj = EditorUtilities.getFileObject(doc);
    if (fObj == null) {
      //do nothing if FO is not attached to the document - the goto would not work anyway:
      return null;
    }
    final AtomicReference<GoToContext> ret = new AtomicReference<GoToContext>();
    doc.render(new Runnable() {
      @Override
      public void run() {
        TokenHierarchy th = TokenHierarchy.get(doc);
        TokenSequence<XMLTokenId> ts = EditorUtilities.getXmlTokenSequence(th, offset);

        if (ts == null) {
          return;
        }

        ts.move(offset);
        if (!ts.moveNext()) {
          return;
        }

        Token<XMLTokenId> t = ts.token();
        String tTxt = t.text().toString();
        if (USABLE_TOKEN_IDS.contains(t.id())) {
          // ignore enclosing quotes
          if (offset >= ts.offset() + 1 && offset < ts.offset() + t.length() - 1) {
            int from = ts.offset() + 1;
            int to = ts.offset() + t.length() - 1;
            Matcher matcher = RES_REF_EXPR.matcher(tTxt);
            if (matcher.matches()) {
                ret.set(GoToContext.toResourceRef(from, to,
                    new ResourceRef(true, null, matcher.group(1), matcher.group(2), null)));
                return;
            }
          
            // check if it is a class name
            Token<XMLTokenId> attrNameToken = findPreceding(XMLTokenId.ARGUMENT, ts);
            if (attrNameToken != null && "android:name".equals(attrNameToken.text().toString())) {
              Token<XMLTokenId> tagNameToken = findPreceding(XMLTokenId.TAG, ts);
              if (tagNameToken != null && MANIFEST_TAGS_WIHT_CLASSES.contains(tagNameToken.text().toString())) {
                ClasspathInfo cpInfo = cpResolver.cpInfoForXmlDocument(doc);
                if (cpInfo != null) {
                  String clzName = tTxt.substring(1, tTxt.length() - 1);
                  ElementHandle<TypeElement> hElement = 
                      ElementHandle.createTypeElementHandle(ElementKind.CLASS, clzName);
                  FileObject fo = cpResolver.findFile(hElement, cpInfo);
                  if (fo != null) {
                    ret.set(GoToContext.toJavaElement(from, to, cpInfo, hElement));
                    return;
                  } else {
                    // try to append after manifest/@package
                    // package tag is only used in manifest
                    Token<XMLTokenId> valueToken;
                    Token<XMLTokenId> nameToken;
                    do {
                      valueToken = findPreceding(XMLTokenId.VALUE, ts);
                      nameToken = findPreceding(XMLTokenId.ARGUMENT, ts);
                      if (valueToken != null && nameToken != null && nameToken.text().toString().equals("package")) {
                        String pkgName = valueToken.text().toString();
                        pkgName = pkgName.substring(1, pkgName.length() - 1);
                        hElement = ElementHandle.createTypeElementHandle(
                            ElementKind.CLASS, 
                            pkgName + (clzName.startsWith(".") ? clzName : "." + clzName));
                        fo = cpResolver.findFile(hElement, cpInfo);
                        if (fo != null) {
                          ret.set(GoToContext.toJavaElement(from, to, cpInfo, hElement));
                          return;
                        }
                      }
                    } while (nameToken != null && valueToken != null);
                    
                  }
                }
              }
            }
          }
        }
        GoToContext widget = tryWidgetNameInLayout(fObj, ts);
        if (widget != null) {
          ret.set(widget);
        }
      }

      private Token<XMLTokenId> findPreceding(XMLTokenId xmlTokenId, TokenSequence<XMLTokenId> ts) {
        Token<XMLTokenId> t = ts.token(); 
        while (t != null && t.id() != xmlTokenId) {
          if (!ts.movePrevious()) {
            return null;
          }
          t = ts.token();
        }
        return t;
      }

      private GoToContext tryWidgetNameInLayout(FileObject fObj, TokenSequence<XMLTokenId> ts) {
        if (!isLayoutFile(fObj)) {
          return null;
        }
        ts.move(offset);
        if (!ts.moveNext()) {
          return null;
        }

        Token<XMLTokenId> t = ts.token();
        String tTxt = t.text().toString();
        if (t.id() == XMLTokenId.TAG) {
          // ignore leading <
          if (offset >= ts.offset() + 1 && offset < ts.offset() + t.length()) {
            int from = ts.offset() + 1;
            int to = ts.offset() + t.length();
            ClasspathInfo cpInfo = cpResolver.cpInfoForXmlDocument(doc);
            String clzName = tTxt.substring(1, tTxt.length());
            ElementHandle<TypeElement> hElement =
                ElementHandle.createTypeElementHandle(ElementKind.CLASS, clzName);
            FileObject fo = cpResolver.findFile(hElement, cpInfo);
            if (fo != null) {
              return GoToContext.toJavaElement(from, to, cpInfo, hElement);
            }
            clzName = "android.widget." + clzName;
            hElement = ElementHandle.createTypeElementHandle(ElementKind.CLASS, clzName);
            fo = cpResolver.findFile(hElement, cpInfo);
            if (fo != null) {
              return GoToContext.toJavaElement(from, to, cpInfo, hElement);
            }
            // TODO findpacakge name in manifest and prepend it
          }
          
        }
        return null;
      }

      private boolean isLayoutFile(FileObject fObj) {
        return fileTypes.isLayoutFile(fObj);
      }
    });
    return ret.get();
  }
}
