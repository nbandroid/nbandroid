package org.nbandroid.netbeans.gradle;

import com.google.common.collect.Iterables;
import java.util.Arrays;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.junit.Assert.*;
import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;

public class LibsTestGradleProjectTest {
  private static GradleProjectFixture prjFix;
  private static Project app;
  private static Project lib;
  private static FileObject foProjectSrc;
  private static PluginsFixture pluginsFix;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("libsTest")
        .useIs(GradleTests.testArchiveStream())
        .create();
    app = prjFix.loadProject("app");
    lib = prjFix.loadProject("lib1");
    
    foProjectSrc = app.getProjectDirectory().getFileObject("src/main/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(app));
    assertTrue(AndroidProjects.isAndroidProject(lib));
    assertFalse(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void basic() throws Exception {
    // get the classpath
    verifyClasspath(app, foProjectSrc, ClassPath.SOURCE, 
        "libsTest/app/src/main/java" 
        // "tictactoe/app/build/source/r/debug" - will be added after build
        );
    
    DalvikPlatformResolver platformProvider = app.getLookup().lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }
  
  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(app);
    assertNotNull(sources);

    final FileObject foSrc = app.getProjectDirectory().getFileObject("src/main/java/com/android/tests/libstest/app/App.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
    final FileObject foRes = app.getProjectDirectory().getFileObject("src/main/resources/com/android/tests/libstest/app/App.txt");
    SourceGroup[] resourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);
    assertTrue("resource in " + sources,
        Iterables.any(
            Arrays.asList(resourceGroups), 
            sourceGroupContainsFile(foRes)));
    final FileObject foLayout = app.getProjectDirectory().getFileObject("src/main/res/layout/main.xml");
    SourceGroup[] sourceGroups2 = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_ANDROID_RES);
    assertTrue("app resource in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups2), 
            sourceGroupContainsFile(foLayout)));
  }
  
  @Test
  public void manifest() throws Exception {
    AndroidManifestSource ams = app.getLookup().lookup(AndroidManifestSource.class);
    assertNotNull(ams);

    FileObject foSrc = app.getProjectDirectory().getFileObject("src/main/" + AndroidConstants.ANDROID_MANIFEST_XML);
    assertEquals(foSrc, ams.get());
  }
}