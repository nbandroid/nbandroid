package org.nbandroid.netbeans.gradle;

import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nbandroid.netbeans.gradle.query.GradleAndroidClassPathProvider;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;

import org.netbeans.junit.MockServices;
import org.netbeans.modules.android.project.api.AndroidProjects;

import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;
/**
 * Negative test using a simple Java project with Gradle to check Android support does not 
 * interfere.
 */
public class JavaGradleProjectExtQueryTest {
  private static final Logger LOG = Logger.getLogger(JavaGradleProjectExtQueryTest.class.getName());
  
  private static GradleProjectFixture prjFix;
  private static FileObject foProjectSrc;
  private static PluginsFixture pluginsFix;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();

    pluginsFix = new PluginsFixture().setupSDK().setupGradle();
    prjFix = new GradleProjectFixture.Builder()
        .useName("gradle-sample")
        .useIs(JavaGradleProjectExtQueryTest.class.getResourceAsStream("gradle-sample.zip"))
        .create();

    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
  }


  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void notAndroidProject() throws Exception {
    assertFalse(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void basic() throws Exception {
    // get the classpath
    verifyClasspath(prjFix.prj, foProjectSrc, ClassPath.SOURCE, "gradle-sample/src/main/java");
    
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNull(platformProvider);
  }
  
  private void verifyClasspath(Project prj, FileObject fo, String cpType, String ... entries) {
    GradleAndroidClassPathProvider acpp = prj.getLookup().lookup(GradleAndroidClassPathProvider.class);
    assertNull(acpp);
    // now we know android is not there but Java is not yet added
  }
}