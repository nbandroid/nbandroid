package org.nbandroid.netbeans.gradle;

import com.android.builder.model.AndroidProject;
import com.google.common.collect.Iterables;
import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nbandroid.netbeans.gradle.ui.DependenciesNode;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectManager;
import org.netbeans.modules.android.core.sdk.DalvikPlatform;
import org.netbeans.modules.android.project.spi.DalvikPlatformResolver;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.java.queries.SourceForBinaryQuery;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.gradle.project.api.task.BuiltInGradleCommandQuery;
import org.netbeans.gradle.project.api.task.GradleCommandTemplate;
import org.netbeans.junit.MockServices;
import org.netbeans.spi.project.ActionProvider;
import org.netbeans.modules.android.project.api.AndroidConstants;
import org.netbeans.modules.android.project.api.AndroidManifestSource;
import org.netbeans.modules.android.project.api.AndroidProjects;
import org.netbeans.modules.android.project.ui.PlatformNode;
import org.netbeans.spi.java.queries.SourceForBinaryQueryImplementation;
import org.netbeans.spi.project.ui.LogicalViewProvider;
import org.openide.nodes.Node;

import static org.nbandroid.netbeans.gradle.GradleProjectFixture.*;
import org.nbandroid.netbeans.gradle.config.AndroidTestRunConfiguration;
import org.nbandroid.netbeans.gradle.query.GradleAndroidClassPathProvider;
import org.netbeans.api.java.classpath.GlobalPathRegistry;

/**
 * Test using basic project from gradle-samples-* found in Android Tools SDK repository.
 */
public class AndroidGradleProjectExtQueryTest {
  private static final Logger LOG = Logger.getLogger(AndroidGradleProjectExtQueryTest.class.getName());

  private static GradleProjectFixture prjFix;
  private static PluginsFixture pluginsFix;
  private static FileObject foProjectSrc;
  private static FileObject foProjectTestSrc;

  @BeforeClass
  public static void setUpClass() throws Exception {
    MockServices.setServices();
    pluginsFix = new PluginsFixture().setupSDK().setupGradle();

    prjFix = new GradleProjectFixture.Builder()
        .usePrefix(GradleTests.TEST_ARCHIVE_DIR)
        .useName("basic")
        .useIs(GradleTests.testArchiveStream())
        .create();
    
    foProjectSrc = prjDir(prjFix).getFileObject("src/main/java");
    foProjectTestSrc = prjDir(prjFix).getFileObject("src/androidTest/java");
  }

  @AfterClass
  public static void clear() {
    prjFix.tearDown();
  }

  @Test
  public void notPureAndroid() throws Exception {
    // there is a src/main/AndroidManifest.xml that can confuse Ant-Android project
    Project prj = ProjectManager.getDefault().findProject(prjDir(prjFix).getFileObject("src/main"));
    assertNull(prj);
  }

  @Test
  public void globalPathRegistry() throws Exception {
    GlobalPathRegistry pathRegistry = GlobalPathRegistry.getDefault();
    assertNotNull(pathRegistry.findResource("com/android/tests/basic/Main.java"));
  }
  
  @Test
  public void isAndroidProject() throws Exception {
    assertTrue(AndroidProjects.isAndroidProject(prjFix.prj));
  }
  
  @Test
  public void basic() throws Exception {
    DalvikPlatformResolver platformProvider = prjLookup(prjFix).lookup(DalvikPlatformResolver.class);
    assertNotNull(platformProvider);
    DalvikPlatform platform = platformProvider.findDalvikPlatform();
    assertNotNull(platform);
  }

  @Test
  public void sourcePath() throws Exception {
    //Build and wait
    prjFix.buildAction().invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, "basic/src/main/java", "basic/build/generated/source/r/debug");
    verifyClasspath(prjFix, foProjectSrc, ClassPath.BOOT, "android.jar");

    //Clean and wait
    prjFix.cleanAction().invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, "basic/src/main/java");
    verifyClasspath(prjFix, foProjectSrc, ClassPath.BOOT, "android.jar");

    //Build and wait
    prjFix.buildAction().invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectSrc, ClassPath.SOURCE, "basic/src/main/java", "basic/build/generated/source/r/debug");
    verifyClasspath(prjFix, foProjectSrc, ClassPath.BOOT, "android.jar");
  }

  @Test
  public void testSourcePath() throws Exception {
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE, "basic/src/androidTest/java");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");

    //Clean and wait
    prjFix.cleanAction().invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE, "basic/src/androidTest/java");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");

    //Build and wait
    prjFix.action(AndroidConstants.COMMAND_BUILD_TEST)
        .andWaitForFile(new File(FileUtil.toFile(prjDir(prjFix)), "build/generated/source/buildConfig/test/debug"))
        .invoke();
    // get the classpath
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.SOURCE, 
        "basic/src/androidTest/java", "basic/build/generated/source/buildConfig/test/debug");
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.COMPILE, 
        "extras/android/m2repository/com/android/support/support-v13/13.0.0/support-v13-13.0.0.jar", 
        "extras/android/m2repository/com/android/support/support-v4/13.0.0/support-v4-13.0.0.jar", 
        "/build/intermediates/exploded-aar/com.google.android.gms/play-services/3.1.36/classes.jar", 
        "basic/build/intermediates/classes/debug" /*, following looks like implementation detail in Gradle
        "basic/build/dependency-cache/debug"*/);
    verifyClasspath(prjFix, foProjectTestSrc, ClassPath.BOOT, "android.jar");
  }
  
  @Test
  public void sources() throws Exception {
    Sources sources = ProjectUtils.getSources(prjFix.prj);
    assertNotNull(sources);

    final FileObject foSrc = 
        prjDir(prjFix).getFileObject("src/main/java/com/android/tests/basic/Main.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_JAVA);
    assertTrue("java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
    final FileObject foRes = 
        prjDir(prjFix).getFileObject("src/main/res/layout/main.xml");
    SourceGroup[] sourceGroups2 = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_ANDROID_RES);
    assertTrue("app resource in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups2), 
            sourceGroupContainsFile(foRes)));
  }
  
  @Test
  public void testSources() throws Exception {
    Sources sources = ProjectUtils.getSources(prjFix.prj);
    assertNotNull(sources);

    final FileObject foSrc = 
        prjDir(prjFix).getFileObject("src/androidTest/java/com/android/tests/basic/MainTest.java");
    SourceGroup[] sourceGroups = sources.getSourceGroups(AndroidConstants.SOURCES_TYPE_INSTRUMENT_TEST_JAVA);
    assertTrue("androidTest java source in " + sources,
        Iterables.any(
            Arrays.asList(sourceGroups), 
            sourceGroupContainsFile(foSrc)));
  }
  
  @Test
  public void manifest() throws Exception {
    AndroidManifestSource ams = prjLookup(prjFix).lookup(AndroidManifestSource.class);
    assertNotNull(ams);

    FileObject foSrc = 
        prjDir(prjFix).getFileObject("src/main/" + AndroidConstants.ANDROID_MANIFEST_XML);
    assertEquals(foSrc, ams.get());
  }
  
  @Test
  public void commands() throws Exception {
    BuiltInGradleCommandQuery commands = prjLookup(prjFix).lookup(BuiltInGradleCommandQuery.class);
    assertNotNull(commands);
    assertTrue(commands.getSupportedCommands().contains(ActionProvider.COMMAND_BUILD));
    assertTrue(commands.getSupportedCommands().contains(ActionProvider.COMMAND_RUN));
    assertTrue(commands.getSupportedCommands().contains(ActionProvider.COMMAND_DEBUG));
    assertTrue(commands.getSupportedCommands().contains(ActionProvider.COMMAND_TEST));
    GradleCommandTemplate testCmd = 
        commands.tryGetDefaultGradleCommand(null, ActionProvider.COMMAND_TEST);
    assertNotNull(testCmd);
    assertTrue(testCmd.getTasks().contains("assemble${BuildVariant}"));
    assertTrue(testCmd.getTasks().contains("assemble${BuildVariant}Test"));
  }
  
  @Test
  public void nodes() {
    LogicalViewProvider viewProvider = prjLookup(prjFix).lookup(LogicalViewProvider.class);
    assertNotNull(viewProvider);
    Node prjNode = viewProvider.createLogicalView();
    Node depsNode = subNodeByDisplayName(prjNode, "Dependencies");
    assertTrue(depsNode instanceof DependenciesNode);
    Node platformNode = subNodeByDisplayName(depsNode, "Android 5.0.1");
    assertTrue(platformNode instanceof PlatformNode);
    Node androidTestNode = subNodeByDisplayName(prjNode, "Instrumentation Tests");
    assertNotNull(androidTestNode);
  }

  @Test
  public void sourceForAndroidLibraryDependency() throws Exception {
    prjFix.buildAction().invoke();
    
    // TODO extract into fixture - post action in actionrunner
    AndroidGradleExtension ext = prjFix.prj.getLookup().lookup(AndroidGradleExtension.class);
    AndroidProject ap = ext.aPrj;
    GradleAndroidClassPathProvider cpp = prjFix.prj.getLookup().lookup(GradleAndroidClassPathProvider.class);
    cpp.setAndroidProject(ap);
    
    URL libRoot = FileUtil.urlForArchiveOrDir(new File(prjFix.prjDir, "build/intermediates/classes/debug"));
    assertNotNull(libRoot);
    SourceForBinaryQueryImplementation sfb = prjLookup(prjFix).lookup(SourceForBinaryQueryImplementation.class);
    SourceForBinaryQuery.Result libSources = sfb.findSourceRoots(libRoot);
    assertNotNull("can resolve compile classes to sources", libSources);
    // check there is main source root of a debug variant
    assertTrue("has main source root in sources: " + Arrays.toString(libSources.getRoots()),
        Arrays.asList(libSources.getRoots()).contains(prjDir(prjFix).getFileObject("src/main/java")));
  }

  
  @Test
  public void testRunner() {
    AndroidTestRunConfiguration testCfg = prjLookup(prjFix).lookup(AndroidTestRunConfiguration.class);
    assertEquals("android.test.InstrumentationTestRunner", testCfg.getTestRunner());
  }
}